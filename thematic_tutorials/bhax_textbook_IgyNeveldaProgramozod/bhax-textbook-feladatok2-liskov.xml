<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Liskov!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>EPAM: Interfész evolúció Java-ban</title>

	<para>
            Leírás forrása: <link xlink:href="https://beginnersbook.com/2017/10/java-8-interface-changes-default-method-and-static-method/">https://beginnersbook.com/2017/10/java-8-interface-changes-default-method-and-static-method/</link>
            <link xlink:href="https://javaconceptoftheday.com/java-8-interface-changes-default-methods-and-static-methods/">https://javaconceptoftheday.com/java-8-interface-changes-default-methods-and-static-methods/</link>
        </para>

        <para>
            A Java 8-as verziója előtt csak absztrakt metódusokat (nem megjeleníthető osztályok, alosztályok) lehetett használni. Az interfészek metódusai alapjáraton publikusak és absztraktok. A Java 8-tól kezdődően azonban már az interfészeknek lehetnek default és statikus metódusai (külső osztályok számára nem elérhető metódusok) is, ezáltal a fejlesztők új metódusokat tudnak adni az interfészhez anélkül, hogy hatással lennének arra az adott osztályra, ami kivitelezi ezeket az interfészeket. Azonban ezek az újítások némi új problémához is vezettek. Ilyen az úgynevezett "gyémánt probléma", ami akkor keletkezik, ha egy osztály több metódust is örököl ugyanazokkal a jellemzőkkel, ami problémához vezethet a programok futásában.
        </para>

        <para>
            Példa az absztrakt metódusra: létrehoztunk egy default interfészt, amiben deklaráltunk egy absztrakt és egy default metódust, ami tartalmaz egy kiíratást. Továbbá létrehoztunk egy osztályt és kiegészítjük a korábban létrehozott interfészzel. Ezután az interfészben létrehozott absztrakt metódust felülírjuk(@Override), ami szintén egy kiíratást fog tartalmazni, a felülírt szöveggel. Így a defaultMethod() implementálása nélkül el tudtuk érni annak tartalmát.
        </para>

	<programlisting language="Java"><![CDATA[       
    interface InterfaceDefault
    {
        void abstractMethod();
        
        default void defaultMethod()
        {
            System.out.println("Ez egy default metódus");
        }
    }
    
    class anyClass implements InterfaceDefault
    {
        @Override
        public void abstractMethod() 
        {
            System.out.println("Absztrakt metódus implementálva");
        }
        
    }
	]]></programlisting>

        <para>
            Példa a statikus metódusra: Ebben a kódcsipetben szintén létrehoztunk egy interfészt, amiben definiáltunk egy default és egy statikus metódust is, amikben kiíratunk egy-egy mondatot. Ezután létrehozunk egy osztályt, amit kibővítettünk az interfésszel, majd felülírtuk az interfész absztrakt metódusát. Így az osztályban már nincs szükség arra, hogy implementáljuk a defaultMethod()-ot, a staticMethod()-ot pedig nem is tudjuk megvalósítani osztályon belül.
        </para>

         <programlisting language="Java"><![CDATA[       
    interface InterfaceDefaultStaticMethod
    {
        void abstractMethod();    
        
        default void defaultMethod()
        {
            System.out.println("Ez egy default metódus");
        }
        
        static void staticMethod()
        {
            System.out.println("Ez egy statikus metódus");
        }
    }
    
    class AnyClass implements InterfaceDefaultStaticMethod
    {
        @Override
        public void abstractMethod() 
        {
            System.out.println("Absztrakt metódus implementálva");
        }
        
        //Nincs szükség a defaultMethod() implementálására
        
        //A staticMethod() -ot nem lehet implementálni
    }
	]]></programlisting>

    </section>

    <section>
        <title>Liskov helyettesítés sértése</title>

	<para>
            Leírás forrása: <link xlink:href="https://hun.small-business-tracker.com/exploring-liskov-substitution-principle-224197">https://hun.small-business-tracker.com/exploring-liskov-substitution-principle-224197</link>
	</para>

	<para>
		Először is a Liskov-féle behelyettesítési elv röviden: Rövidebb nevén az LSP(Liskov Subtitution Principle), kimondja, hogy a program viselkedése nem változhat meg attól, hogy az ős osztály egy példánya helyett a jövőben valamelyik gyermek osztályának példányát használom. Azaz a program által visszaadott érték nem függ attól, hogy egy Kacsa vagy egy Liba vagy egy Vizicsirke példány lábainak számát adom vissza.
	</para>	
	
	<para>
		Nézzünk egy példát, amely nem felel meg az LSP elvnek. A klasszikus ellenpélda az ellipszis – kör illetve a téglalap – négyzet példa. A kör olyan speciális ellipszis, ahol a két sugár egyenlő. A négyzet olyan speciális téglalap, ahol az oldalak egyenlő hosszúak. Szinte adja magát, hogy a kör az ellipszis alosztálya, illetve a négyzet a téglalap alosztálya legyen. 
	</para>

	<para>
            Nézzük is a Liskov elvét sértő programot C++ -ban. A program témája a madarakon keresztül mutatja be miről is szól a Liskov elv sértése, hiszen a madarakat különféle szempontok alapján meg lehet különböztetni. A programban ez a megkülönböztetés repülőkészségük alapján történik.
        </para>

	<para>
            Létrehozzuk a Madár és Program osztályunkat.
	</para>

        <programlisting language="c"><![CDATA[
    #include <iostream>
    using namespace std;
       
    class Madar {
    public:
        virtual void repul() {}
    };

    class Program {
    public:
        void fuggveny(Madar& madar) {
            madar.repul();
        }
    };


    class Solyom : public Madar{};

    class Pingvin : public Madar {};
	]]></programlisting>

	<para>
		A Madár osztályon belül látható egy "virtual void" függvény. A "virtual" kulcsszóval jelezzük, hogy más osztályba felül lesz definiálva ez a függvény. A Program osztályon belül látható szintén egy void függvény létrehozása, aminek paraméterként átadunk egy Madár objektumot referenciaként (egy hivatkozást kap arra a területre, ahol található az objektum). Ezen a függvényen belül a korábban definiált függvényhívás történik.
        </para>

        <para>
            Majd deklarálunk szintén 2 osztályt, amik madártípusok. Ezek közül is az egyik madár tud repülni(Sólyom), a másik röpképtelen(Pingvin). Ezeket az osztályokat származtatjuk a Madár osztályból, tehát örökölni fogják a tulajdonságait.
        </para>
        
	<para>
            Majd jön a main():
	</para>

        <programlisting language="c"><![CDATA[       
   int main()
    {
        Program progi;
        Madar madar;
        progi.fuggveny(madar);
        Solyom solyom;
        progi.fuggveny(solyom);
        Pingvin pingvin;
        progi.fuggveny(pingvin);
    }
	]]></programlisting>

	<para>
		A main() függvényben látható a osztályókból egy-egy példány létrehozatala és a Program osztályban látható függvény meghivása az egyedekre.
        </para>
        
	<para>
            Ebben az esetben lefordul a program, nincs hiba a programban, ami nyelvtanilag nem lenne helyes a C++ -os fordító számára. Jobban megfigyelve a programot, a pingvinnél is meghívásra kerül a repül függvény, amit a Madár osztályból származtattunk. Ezt az egyednek lehetetlen kivitelezni. Itt mutatkozik meg a Liskov elv sértése.
        </para>

        <para>
            Mi a megoldás?
        </para>

        <para>
            Erre a válasz nagyon egyszerű. Jobban kell tervezni programunkat, ami a mi esetünkben egy Repülő madár osztály deklarálása jelenti. Ezt az osztályt származtatjuk a Madár osztályból. A további két osztállyal is hasonlóan teszünk. A Pingvin osztályt zármaztatjuk a Madár osztálból, a Sólyom osztályt pedig a Repülő madár osztályból. Ezáltal futóképes programot kappunk a Liskov el sértése nélkül.
        </para>

        <programlisting language="c"><![CDATA[       
    #include <iostream>
    using namespace std;

    class Madar {};

    class repulo_Madar: public Madar {
    public:
        virtual void repul() {}
    };


    class Program {
    public:
        void fuggveny(Madar& madar) {
        
        }

    };

    class Solyom : public repulo_Madar {};

    class Pingvin : public Madar {};


    int main()
    {
        Program progi;
        Madar madar;
        progi.fuggveny(madar);
        Solyom solyom;
        progi.fuggveny(solyom);
        Pingvin pingvin;
        progi.fuggveny(pingvin);

    }
	]]></programlisting>

        <para>
            A C++ -os verzió után nézzük meg a Java -s verziót is, ami a következő, azaz a 13.4-es feladat.
        </para>

    </section>        

    <section>
        <title>EPAM: Liskov féle helyettesíthetőség elve, öröklődés</title>

	<para>
            Megoldás forrása: <link xlink:href="https://github.com/epam-deik-cooperation/epam-deik-prog2/tree/master/week-2/liskov/src/main/java/com/epam/training">https://github.com/epam-deik-cooperation/epam-deik-prog2/tree/master/week-2/liskov/src/main/java/com/epam/training</link>
	</para>

	<para>
		Most, pedig nézzük egy példát Java nyelven.
	</para>

	<para>Vehicle.java</para>

         <programlisting language="Java"><![CDATA[ 
package com.epam.training;

public class Vehicle {

    public Vehicle() {
        System.out.println("Creating vehicle!");
    }

    void start() {
        System.out.println("Vehicle is starting!");
    }

}
	]]></programlisting>

	<para>Car.java</para>

         <programlisting language="Java"><![CDATA[
package com.epam.training;

public class Car extends Vehicle {

    public Car() {
        System.out.println("Creating car!");
    }

    @Override
    void start() {
        System.out.println("Car is starting!");
    }

}
	]]></programlisting> 

	<para>Supercar.java</para>

         <programlisting language="Java"><![CDATA[
package com.epam.training;

public class Supercar extends Car {

    public Supercar() {
        System.out.println("Creating supercar!");
    }

    @Override
    void start() {
        System.out.println("Supercar is starting!");
    }

}
	]]></programlisting>

	<para>
		Mi történik ezen kódok futtatása esetén, és miért?
	</para>
	
	<para>Main.java</para>
	<programlisting language="Java"><![CDATA[
package com.epam.training;

public class Main {

    public static void main(String [] args) {
	//1. rész
        Vehicle firstVehicle = new Supercar();
        firstVehicle.start();
        System.out.println(firstVehicle instanceof Car);
	//2.rész
        Car secondVehicle = (Car) firstVehicle;
        secondVehicle.start();
        System.out.println(secondVehicle instanceof Supercar);
	//3.rész
        Supercar thirdVehicle = new Vehicle();
        thirdVehicle.start();
    }
}
	]]></programlisting>

	<para>A magyarázat miatt 3 részre bontottam a programot.</para>
	
	<para>1.rész</para>

	<para>
		Az első jáművünk, amit létre szeretnénk hozni teljesen megfelel az LSP elveknek. Miért? Mert az ős osztály egy példánya helyett valamelyik gyermek osztályának példányát használhatom. Ezért nem megyünk hibába, hogy new Supercart hozunk létre, mikor elötte Vehicle osztályú firstVehicle-t akarunk létrehozni.
	</para>

	<para>
		Fontos még itt megjegyezni, hogy az instanceof ellenőrzi, hogy az adott objektum egy másik osztály példánya-e vagy nem. Ezért kapunk majd egy true -t a kód futtatása közben.
	</para>

	<para>Tehát a programunk eddig ezeket adja nekünk vissza:</para>
	<programlisting language="Java"><![CDATA[
	Creating vehicle!
	Creating car!
	Creating supercar!
	Supercar is starting!
	true
	]]></programlisting>

	<para>2.rész</para>

	<para>
		A második járművünk létrehozásánál már kicsit másabbak a dolgok. Létre hozunk egy Car típusú objektumot, aminek próbáljuk értékül adni a korábban létrehozott firstVehicle-t. Tehát típuskényszeríteni szeretnénk. De az így létrehozott secondVehicle nem Car típusú lesz, hanem Supercar. Miért? Mert nem egy új objektumot hoztunk létre (new ...), hanem átmásoltuk, a korábban létrehozott firstVehicle-t. 
	</para>

	<para>Tehát a programunk eddig ezeket adja nekünk vissza:</para>
	<programlisting language="Java"><![CDATA[
	Supercar is starting!
	true
	]]></programlisting>

	<para>3.rész</para>

	<para>
		A harmadik járművünk létrehozásánál már vannak kisebb problémák. Vehicle objektumra szeretnénk létrehozni, egy Supercar példányt. Ami nem lehetséges LSP miatt. Miért? Az ős osztály nem tudja elérni a származtatott osztály tulajdonságait, ezért ez a része a kódunknak le sem fordul.
	</para>

    </section>


</chapter>                
